"""
Purpose: To test the acp_times.open_time() function.
Author: Adam Case
"""
from acp_times import *
from nose.tools import *

import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.WARNING)
log = logging.getLogger(__name__)
"""
1000km ACP BREVET
Checkpoint       Date  Time
==========       ====  ====
    0km   start: 11/08 00:00
          close: 11/08 01:00

   60km    open: 11/08 01:46
          close: 11/08 04:00

  125km    open: 11/08 03:41
          close: 11/08 08:20

  200km    open: 11/08 05:53
          close: 11/08 13:20

  230km    open: 11/08 06:49
          close: 11/08 15:20

  500km    open: 11/08 15:28
          close: 11/09 09:20

  600km    open: 11/08 18:48
          close: 11/09 16:00

  900km    open: 11/09 05:31
          close: 11/10 18:15

 1000km    open: 11/09 09:05
          close: 11/11 03:00
"""
brevet_dist = 1000
brevet_start_time = arrow.get(2019, 11, 8)
control_dist_km = [60, 125, 200, 230, 500, 600, 900, 1000]
oracle_open_time = [(0, 1, 46), (0, 3, 41), (0, 5, 53), (0, 6, 49), (0, 15, 28), (0, 18, 48), (1, 5, 31), (1, 9, 5)]
oracle_close_time = [(0, 4, 0), (0, 8, 20), (0, 13, 20), (0, 15, 20), (1, 9, 20), (1, 16, 0), (2, 18, 15), (3, 3, 0)]

"""
200km ACP BREVET
Checkpoint       Date  Time
==========       ====  ====
    0km   start: 11/08 00:00
          close: 11/08 01:00

  136km    open: 11/08 04:00
          close: 11/08 09:04

  213km    open: 11/08 05:53
          close: 11/08 13:30
"""
short_brevet_dist = 200
short_constrol_dist_km = [136, 213]
oracle_short_open_time = [(0, 4, 0), (0, 5, 53)]
oracle_short_close_time = [(0, 9, 4), (0, 13, 30)]

def test_open_time():
    """
    Testing a full brevets opening times for a 1000km brevet.
    """
    log.debug(f'Running 1000km brevet')
    for index, control_dist in enumerate(control_dist_km):
        log.debug(f'Running opening time for controle point at {control_dist}km')
        day, hour, minute = oracle_open_time[index]
        opening = brevet_start_time.shift(days=day, hours=hour, minutes=minute).isoformat()
        eq_(opening, open_time(control_dist, brevet_dist, brevet_start_time.isoformat()))

    log.debug(f'Running 200km brevet')
    for index, control_dist in enumerate(short_constrol_dist_km):
        log.debug(f'Running opening time for controle point at {control_dist}km')
        day, hour, minute = oracle_short_open_time[index]
        opening = brevet_start_time.shift(days=day, hours=hour, minutes=minute).isoformat()
        eq_(opening, open_time(control_dist, short_brevet_dist, brevet_start_time.isoformat()))

def test_close_time():
    """
    Testing a full brevets closing times
    """
    log.debug(f'Running 1000km brevet')
    for index, control_dist in enumerate(control_dist_km):
        log.debug(f'Running closing time for controle point at {control_dist}km')
        day, hour, minute = oracle_close_time[index]
        closing = brevet_start_time.shift(days=day, hours=hour, minutes=minute).isoformat()
        assert closing == close_time(control_dist, brevet_dist, brevet_start_time.isoformat())

    log.debug(f'Running 200km brevet')
    for index, control_dist in enumerate(short_constrol_dist_km):
        log.debug(f'Running opening time for controle point at {control_dist}km')
        day, hour, minute = oracle_short_close_time[index]
        closing = brevet_start_time.shift(days=day, hours=hour, minutes=minute).isoformat()
        eq_(closing, close_time(control_dist, short_brevet_dist, brevet_start_time.isoformat()))
