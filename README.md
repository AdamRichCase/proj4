# Project 4: Brevet time calculator with Ajax

Reimplementation of the
[RUSA ACP controle time calculator](https://rusa.org/octime_acp.html)
with flask and ajax.
# Authors
* Michal Young: Initial version of this code.
* Ram Durairajan: Provision of the code and instruction for the project.
* Adam Case (acase@uoregon.edun): Implementation of the project.

# Brevet Controle Points
## Opening and Closing Time Calculation

The algorithm for calculating controle times is described [here](https://rusa.org/pages/acp-brevet-control-times-calculator). The table below is copied from associated link, the last row is not
included because brevets by acp are not greated than 1000km.

| Control Location (km) | Minimum Speed (km/hr)	| Maximum Speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
|        (0 - 200]      |          15           |          34           |
|      (200 - 400]      |          15           |          32           |
|      (400 - 600]      |          15           |          30           |
|      (600 - 1000]     |        11.428         |          38           |
NOTE: ( and ] follow standard interval notation

Brevets are 200, 300, 400, 600, or 1000km in length. Control points can be up
to 20% past the brevet distance, but never exceed 1000km.

 The controle point opening time is calculated by the sum time of distance traveled
 in a controle location range divided by the maximum speed, see Example 1.
 The time to open is rounded to the nearest minute.
 If the controle point is past the brevet length the opening time is calculated
 using the brevet length, see Example 2.


 The controle point closing time is the same as the opening time, just the
 minimum speed is used in place of the maximum speed. Except for the closing
 times for 200km brevets which have a closing time of 13H30 even though the
 calculation results in 13H20.

## Examples
In these examples opening times are calculated, but the same process is followed
for calculating closing times.
### Example 1 (Controle Point Covering Multiple Controle Ranges)

Let a brevet be 300km.

If a controle point is setup at 150km, then the opening time will be 150/34 =
4.411 hours = 264.7 minutes which will be rounded to 265 minutes from the start
of the brevet.

If a second controle point is setup at 220km, then the opening time will be
200/34 + 20/32 = 6.507 hours = 390.4 minutes which will be rounded to 390
minutes from the start of the brevet.

### Example 2 (Controle Point Beyone Brevet Distance)

Let a brevet be 200km.

If a final controle point is setup at 204km, 4km past the brevet distance, this
is okay, but the calculation is not the same as shown in Example 1.
The calculation for the opening time would be 204/34 = 6 hours = 360 minutes
from the start of the brevet.

## Guidence

It is not recommended to place a controle point at or before the 15 km mark of
the brevet as since the start of the brevet is typically open for 1 hour the
closing of the brevet start location will occur before the closure of the
first controle point.

# Developers
## Docker
All dependancies are in the `Dockerfile` and the web applicaiton is able to be
built and ran using Docker.

## Frontend
The template files can be found in `templates/`. `calc.html` contains
Javascript that automatically converts controle point entries from km to miles
and vise versa. The template will send the brevet length, controle point
distance, and date/time in ISO 8601 format to the backend where an ISO 8601
formatted date/time is returned by the backend and then populated in the
appropraite field.

## Backend
The backend consists of two main files `flask_brevets.py` and `acp_times.py`.

`flask_brevets.py`: Contains the flask microframework that manages routes when
navigating to the page, errors, and calls made by the frontend.

`acp_times.py`: Contains the individual calculations for opening and closing
times. This seperation is important for testing listed below.

## Testing
A suite of nose test cases can be found in `tests/` which tests the backend of
the server. The tested functions are `open_time` and `close_time` contained in
`acp_times.py`. These unittests on thes functions are to ensure that the proper
open or closing time is provided.

All tests can be ran while in the container using `nosetests3 tests/` as long
as you are in the `/app` directory.

## Tasks

The code under "brevets" can serve as a starting point. It illustrates a very simple Ajax transaction between the Flask server and javascript on the web page. At present the server does not calculate times. It just returns double the number of miles. Other things may be missing; add them as needed. As before, you should fork and then clone the bitbucket repository, make your changes, and turn in the URL of your repository.

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name, email, etc.) but but also a revised, clear specification of the brevet controle time calculation rules.

* An automated 'nose' test suite.

* Dockerfile

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend. That is, open and close times are automatically populated,
	* Logic in the backend (acp_times.py),
	* Frontend to backend interaction (with correct requests/responses),
	* README is updated with a clear specification, and
	* All five tests pass.

* If the AJAX logic is not working, 10 points will be docked off.

* If the README is not clear or missing, up to 15 points will be docked off.

* If the test cases fail, up to 15 points will be docked off.

* If the logic in the acp_times.py file is wrong or is missing in the appropriate location, 30 points will be docked off.

* If none of the functionalities work, 30 points will be given assuming
    * The credentials.ini is submitted with the correct URL of your repo, and
    * The Dockerfile builds without any errors

* If the Dockerfile doesn't build or is missing, 10 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.
